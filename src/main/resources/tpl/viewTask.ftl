[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[@ww.label labelKey='executable.type' name='label' /]
[@ww.label labelKey='phing.target' name='target' /]

[@ww.label labelKey="phing.config" name="config" hideOnNull='true' /]
[@ww.label labelKey="phing.arguments" name="arguments" hideOnNull='true' /]
[@ww.label labelKey='builder.common.sub' name='workingSubDirectory' hideOnNull='true' /]

[#if hasTests ]
    [@ww.label labelKey='builder.common.tests.directory' name='testResultsDirectory' hideOnNull='true' /]
[/#if]
